package com.ar.experta.pruebaTecnica.exception;

/**
 * Created by Ale on 5/10/2018.
 */
public class BusinessException extends Exception {
    public BusinessException(String msg) {
        super(msg);
    }
}
