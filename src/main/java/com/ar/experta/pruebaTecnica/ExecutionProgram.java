package com.ar.experta.pruebaTecnica;

import com.ar.experta.pruebaTecnica.domain.Response;
import com.ar.experta.pruebaTecnica.domain.inputData.InputData;
import com.ar.experta.pruebaTecnica.domain.matrix.Matrix;
import com.ar.experta.pruebaTecnica.enums.ResponseStatus;
import com.ar.experta.pruebaTecnica.exception.BusinessException;
import com.ar.experta.pruebaTecnica.service.MatrixService;

/**
 * Created by Ale on 5/10/2018.
 */
public class ExecutionProgram {

    public final static String UPDATE = "UPDATE";
    public final static String QUERY = "QUERY";

    private MatrixService matrixService;

    public ExecutionProgram(MatrixService matrixService) {
        this.matrixService = matrixService;
    }

    public Response execute(String executionType, Matrix matrix, InputData inputData) {

        Integer sum = null;

        try {

            switch (executionType) {
                case UPDATE:
                    matrixService.update(inputData, matrix);
                    break;
                case QUERY:
                    sum = matrixService.query(inputData, matrix);
                    break;
            }

            Response response = new Response(ResponseStatus.SUCCESS.name(), sum);
            return response;

        } catch (BusinessException ex) {
            Response response = new Response(ResponseStatus.ERROR.name(), sum);
            return response;
        }
    }
}
