package com.ar.experta.pruebaTecnica.domain.matrix;

import com.ar.experta.pruebaTecnica.domain.inputData.InputData;

/**
 * Created by Ale on 5/10/2018.
 */
public abstract class Matrix {
    public abstract Integer getSize();
    public abstract void update(InputData inputData);
    public abstract Integer query(InputData inputData);
}
