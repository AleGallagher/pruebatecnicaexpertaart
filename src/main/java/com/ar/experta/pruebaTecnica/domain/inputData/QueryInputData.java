package com.ar.experta.pruebaTecnica.domain.inputData;

import com.ar.experta.pruebaTecnica.domain.validation.MatrixValidator;

/**
 * Created by Ale on 5/10/2018.
 */
public class QueryInputData extends InputData {

    private Integer x1;
    private Integer x2;
    private Integer y1;
    private Integer y2;
    private Integer z1;
    private Integer z2;

    public QueryInputData(Integer x1, Integer y1, Integer z1, Integer x2, Integer y2, Integer z2) {
        this.x1 = x1;
        this.y1 = y1;
        this.z1 = z1;
        this.x2 = x2;
        this.y2 = y2;
        this.z2 = z2;
    }

    public Integer getX1() {
        return x1;
    }

    public Integer getX2() {
        return x2;
    }

    public Integer getY1() {
        return y1;
    }

    public Integer getY2() {
        return y2;
    }

    public Integer getZ1() {
        return z1;
    }

    public Integer getZ2() {
        return z2;
    }

    @Override
    public boolean isValid(Integer size) {
        if (MatrixValidator.isIndexPairValid(this.getX1(), this.getX2(), 0, size) &&
                MatrixValidator.isIndexPairValid(this.getY1(), this.getY2(), 0, size) &&
                MatrixValidator.isIndexPairValid(this.getZ1(), this.getZ2(), 0, size)) {
            return true;
        }
        return false;
    }
}
