package com.ar.experta.pruebaTecnica.domain.validation;

/**
 * Created by Ale on 5/10/2018.
 */
public class MatrixValidator {

    public static boolean isValueValid(Integer value, Integer min, Integer max) {
        if (value >= min && value < max) {
            return true;
        }
        return false;
    }

    public static boolean isIndexPairValid(Integer index1, Integer index2, Integer min, Integer max) {
        if (index1 >= min && index2 >= min && index1 < max && index2 < max && index1 < index2) {
            return true;
        }
        return false;
    }

}
