package com.ar.experta.pruebaTecnica.domain.inputData;

/**
 * Created by Ale on 5/10/2018.
 */
public abstract class InputData {
    public abstract boolean isValid(Integer matrixSize);
}
