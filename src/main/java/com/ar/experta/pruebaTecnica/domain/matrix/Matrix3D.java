package com.ar.experta.pruebaTecnica.domain.matrix;

import com.ar.experta.pruebaTecnica.domain.inputData.InputData;
import com.ar.experta.pruebaTecnica.domain.inputData.QueryInputData;
import com.ar.experta.pruebaTecnica.domain.inputData.UpdateInputData;

/**
 * Created by Ale on 5/10/2018.
 */
public class Matrix3D extends Matrix {

    int[][][] matrix;

    public Matrix3D(Integer size) {
        this.matrix = new int[size][size][size];
    }

    @Override
    public Integer getSize() {
        return matrix.length;
    }

    @Override
    public void update(InputData inputData) {

        UpdateInputData updateInputData = (UpdateInputData) inputData;

        this.matrix[updateInputData.getX()][updateInputData.getY()][updateInputData.getZ()] = updateInputData.getValue();
    }

    @Override
    public Integer query(InputData inputData) {

        QueryInputData queryInputData = (QueryInputData) inputData;
        Integer sum = 0;

        for (int i = queryInputData.getX1(); i <= queryInputData.getX2(); i++) {
            for (int j = queryInputData.getY1(); j <= queryInputData.getY2(); j++) {
                for (int k = queryInputData.getZ1(); k <= queryInputData.getZ2(); k++) {
                    sum += this.matrix[i][j][k];
                }
            }
        }

        return sum;
    }
}
