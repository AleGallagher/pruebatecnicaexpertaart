package com.ar.experta.pruebaTecnica.domain;

/**
 * Created by Ale on 5/10/2018.
 */
public class Response {

    private String status;
    private Integer sum;

    public Response(String status, Integer sum) {
        this.status = status;
        this.sum = sum;
    }

    public String getStatus() {
        return status;
    }

    public Integer getSum() {
        return sum;
    }
}
