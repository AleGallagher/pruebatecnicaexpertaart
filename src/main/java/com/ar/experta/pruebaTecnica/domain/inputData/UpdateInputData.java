package com.ar.experta.pruebaTecnica.domain.inputData;

import com.ar.experta.pruebaTecnica.domain.validation.MatrixValidator;

/**
 * Created by Ale on 5/10/2018.
 */
public class UpdateInputData extends InputData {

    public static final Integer MIN_VALUE_VALID = -126;
    public static final Integer MAX_VALUE_VALID = 126;

    Integer x;
    Integer y;
    Integer z;
    Integer value;

    public UpdateInputData(Integer x, Integer y, Integer z, Integer value) {
        this.x = x;
        this.y = y;
        this.z = z;
        this.value = value;
    }

    public Integer getX() {
        return x;
    }

    public Integer getY() {
        return y;
    }

    public Integer getZ() {
        return z;
    }

    public Integer getValue() {
        return value;
    }

    @Override
    public boolean isValid(Integer matrixSize) {
        if (MatrixValidator.isValueValid(this.getValue(), MIN_VALUE_VALID, MAX_VALUE_VALID) &&
                MatrixValidator.isValueValid(this.getX(), 0, matrixSize) &&
                MatrixValidator.isValueValid(this.getY(), 0, matrixSize) &&
                MatrixValidator.isValueValid(this.getZ(), 0, matrixSize)) {
            return true;
        }
        return false;
    }
}
