package com.ar.experta.pruebaTecnica.enums;

/**
 * Created by Ale on 5/10/2018.
 */
public enum ResponseStatus {
    SUCCESS("SUCCESS"),
    ERROR("ERROR");

    private String operation;

    ResponseStatus(String operation) {
        this.operation = operation;
    }
}
