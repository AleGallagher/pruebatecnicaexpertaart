package com.ar.experta.pruebaTecnica.service.impl;

import com.ar.experta.pruebaTecnica.domain.matrix.Matrix;
import com.ar.experta.pruebaTecnica.domain.inputData.InputData;
import com.ar.experta.pruebaTecnica.exception.BusinessException;
import com.ar.experta.pruebaTecnica.service.MatrixService;

/**
 * Created by Ale on 5/10/2018.
 */
public class MatrixServiceImpl implements MatrixService {

    public MatrixServiceImpl() {
    }

    public void update(InputData inputData, Matrix matrix) throws BusinessException {

        if (!inputData.isValid(matrix.getSize())) {
            throw new BusinessException("Invalid data");
        }
        matrix.update(inputData);
    }

    public Integer query(InputData inputData, Matrix matrix) throws BusinessException {

        if (!inputData.isValid(matrix.getSize())) {
            throw new BusinessException("Invalid data");
        }
        return matrix.query(inputData);
    }
}
