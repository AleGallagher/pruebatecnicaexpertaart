package com.ar.experta.pruebaTecnica.service;

import com.ar.experta.pruebaTecnica.domain.matrix.Matrix;
import com.ar.experta.pruebaTecnica.domain.inputData.InputData;
import com.ar.experta.pruebaTecnica.exception.BusinessException;

/**
 * Created by Ale on 5/10/2018.
 */
public interface MatrixService {
    void update(InputData inputData, Matrix matrix) throws BusinessException;

    Integer query(InputData inputData, Matrix matrix) throws BusinessException;
}
