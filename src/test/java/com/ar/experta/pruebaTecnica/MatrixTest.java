package com.ar.experta.pruebaTecnica;

import com.ar.experta.pruebaTecnica.domain.Response;
import com.ar.experta.pruebaTecnica.domain.inputData.InputData;
import com.ar.experta.pruebaTecnica.domain.inputData.QueryInputData;
import com.ar.experta.pruebaTecnica.domain.inputData.UpdateInputData;
import com.ar.experta.pruebaTecnica.domain.matrix.Matrix;
import com.ar.experta.pruebaTecnica.domain.matrix.Matrix3D;
import com.ar.experta.pruebaTecnica.enums.ResponseStatus;
import com.ar.experta.pruebaTecnica.service.MatrixService;
import com.ar.experta.pruebaTecnica.service.impl.MatrixServiceImpl;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created by Ale on 5/10/2018.
 */
public class MatrixTest {

    public final static String UPDATE = "UPDATE";
    public final static String QUERY = "QUERY";

    @Test
    public void whenUpdateAMatrixWithAValidIndexTheStatusShouldBeSuccess() {

        Matrix matrix = new Matrix3D(3);
        MatrixService matrixService = new MatrixServiceImpl();
        ExecutionProgram executionProgram = new ExecutionProgram(matrixService);

        InputData inputData = new UpdateInputData(0, 0, 0, 1);

        Response response = executionProgram.execute(UPDATE, matrix, inputData);

        Assert.assertEquals(ResponseStatus.SUCCESS.name(), response.getStatus());
        Assert.assertNull(response.getSum());
    }

    @Test
    public void whenUpdateAMatrixWithAnInvalidIndexTheStatusShouldBeError() {

        Matrix matrix = new Matrix3D(3);
        MatrixService matrixService = new MatrixServiceImpl();
        ExecutionProgram executionProgram = new ExecutionProgram(matrixService);

        InputData inputData = new UpdateInputData(0, 4, 0, 1);

        Response response = executionProgram.execute(UPDATE, matrix, inputData);

        Assert.assertEquals(ResponseStatus.ERROR.name(), response.getStatus());
        Assert.assertNull(response.getSum());
    }

    @Test
    public void whenUpdateAMatrixWithAnInvalidValueTheStatusShouldBeError() {

        Matrix matrix = new Matrix3D(3);
        MatrixService matrixService = new MatrixServiceImpl();
        ExecutionProgram executionProgram = new ExecutionProgram(matrixService);

        InputData inputData = new UpdateInputData(0, 0, 0, 300);

        Response response = executionProgram.execute(UPDATE, matrix, inputData);

        Assert.assertEquals(ResponseStatus.ERROR.name(), response.getStatus());
        Assert.assertNull(response.getSum());
    }

    @Test
    public void whenQueryAnEmptyAMatrixWithAValidIndexesTheStatusShouldBeSuccessAndTheSumShouldBe0() {

        Matrix matrix = new Matrix3D(3);
        MatrixService matrixService = new MatrixServiceImpl();
        ExecutionProgram executionProgram = new ExecutionProgram(matrixService);

        InputData inputData = new QueryInputData(0, 0, 0, 2, 2, 2);

        Response response = executionProgram.execute(QUERY, matrix, inputData);

        Assert.assertEquals(ResponseStatus.SUCCESS.name(), response.getStatus());
        Assert.assertEquals(Long.valueOf(0), Long.valueOf(response.getSum()));
    }

    @Test
    public void whenQueryAnUpdatedAMatrixWithAValidIndexesTheStatusShouldBeSuccessAndTheSumShouldBe3() {

        Matrix3D matrix = new Matrix3D(3);
        MatrixService matrixService = new MatrixServiceImpl();
        ExecutionProgram executionProgram = new ExecutionProgram(matrixService);

        InputData inputUpdateData = new UpdateInputData(0, 0, 0, 3);
        InputData inputQueryData = new QueryInputData(0, 0, 0, 2, 2, 2);

        executionProgram.execute(UPDATE, matrix, inputUpdateData);

        Response response = executionProgram.execute(QUERY, matrix, inputQueryData);

        Assert.assertEquals(ResponseStatus.SUCCESS.name(), response.getStatus());
        Assert.assertEquals(Long.valueOf(3), Long.valueOf(response.getSum()));
    }

    @Test
    public void whenQueryAMatrixWithInvalidIndexesTheStatusShouldBeError() {

        Matrix matrix = new Matrix3D(3);
        MatrixService matrixService = new MatrixServiceImpl();
        ExecutionProgram executionProgram = new ExecutionProgram(matrixService);

        InputData inputData = new QueryInputData(5, 0, 0, 2, 2, 2);

        Response response = executionProgram.execute(QUERY, matrix, inputData);

        Assert.assertEquals(ResponseStatus.ERROR.name(), response.getStatus());
    }
}
