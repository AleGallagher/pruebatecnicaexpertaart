# Prueba técnica Experta

#### 1. Run Up
To run the application:
- Clonar el master del proyecto
- Correr: mvn test

#### 2. Proyecto
El proyecto cuenta con una clase llamada ExecutionProgram que tiene un metodo llamado "execute". Este metodo recibe como parametro el tipo de operación a realizar (QUERY o UPDATE), la matriz y los datos de entrada.

--Datos de Entrada

Como datos de entrada se tienen dos tipos:
- UpdateInputData:
  Datos de entrada para la operación UPDATE.
  Cuenta con los datos: X, Y, Z y valor de la celda

- QueryInputData:
  Datos de entrada para la operación QUERY.
  Cuenta con los datos: X1,X2,Y1,Y2,Z1,Z2

-- Respuesta

Para el caso de UPDATE el metodo devuelve el status de la operación (SUCCESS o ERROR) como status.

Para el caso de QUERY el metodo devuelve el status de la operación (SUCCESS o ERROR) y el resultado de la suma.

-- Test

Para validar el correcto funcionamiento el proyecto cuenta con los siguientes tests:
- whenUpdateAMatrixWithAValidIndexTheStatusShouldBeSuccess
- whenUpdateAMatrixWithAnInvalidIndexTheStatusShouldBeError
- whenUpdateAMatrixWithAnInvalidValueTheStatusShouldBeError
- whenQueryAnEmptyAMatrixWithAValidIndexesTheStatusShouldBeSuccessAnd0AsTheSum
- whenQueryAnUpdatedAMatrixWithAValidIndexesTheStatusShouldBeSuccessAnd3AsTheSum
- whenQueryAMatrixWithInvalidIndexesTheStatusShouldBeError

